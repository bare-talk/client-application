import 'package:client_application/domain/core/failure/value_failure.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'username_failure.freezed.dart';

@freezed
abstract class UsernameFailure extends ValueFailure<String>
    with _$UsernameFailure {
  const factory UsernameFailure.invalidUsername({required String username}) =
      InvalidEmail;
}
