import 'package:client_application/domain/authentication/failure/username_failure.dart';
import 'package:client_application/domain/core/failure/value_failure.dart';
import 'package:dartz/dartz.dart';

const int minimumLength = 8;
const int maximumLength = 24;

Either<ValueFailure<String>, String> validateUsername(String input) {
  final int inputLength = input.length;

  if (inputLength < minimumLength ||
      inputLength > maximumLength ||
      input.isEmpty) {
    return left(UsernameFailure.invalidUsername(username: input));
  }

  return right(input);
}
