import 'package:client_application/domain/authentication/validation/username_validator.dart';
import 'package:client_application/domain/core/failure/value_failure.dart';
import 'package:client_application/domain/core/value_objects/abstract_value_object.dart';
import 'package:dartz/dartz.dart';

class Username extends ValueObject<String> {
  factory Username(String input) {
    return Username._(validateUsername(input));
  }

  const Username._(this.value);

  @override
  final Either<ValueFailure<String>, String> value;
}
