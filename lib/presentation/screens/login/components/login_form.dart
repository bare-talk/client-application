import 'package:client_application/presentation/shared/components/scrollable_padded_list.dart';
import 'package:client_application/presentation/shared/components/vertical_space.dart';
import 'package:client_application/presentation/shared/forms/inputs/text_input.dart';
import 'package:flutter/material.dart';

class LoginForm extends StatefulWidget {
  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _usernameController = TextEditingController();

  @override
  void initState() {
    _usernameController.addListener(() => setState(() => {}));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: ScrollablePaddedList(
        children: [
          const VerticalSpace(),
          UsernameFormInput(
            controller: _usernameController,
            validator: (value) {},
          ),
        ],
      ),
    );
  }
}
