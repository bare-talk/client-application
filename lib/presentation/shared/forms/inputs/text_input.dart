import 'package:client_application/presentation/shared/buttons/close_and_reset_button.dart';
import 'package:client_application/presentation/shared/forms/inputs/abstract_text_input.dart';
import 'package:flutter/material.dart';

class UsernameFormInput extends AbstractTextInput {
  const UsernameFormInput({
    Key? key,
    required TextEditingController controller,
    required FormFieldValidator<String> validator,
  }) : super(key: key, controller: controller, validator: validator);

  @override
  _TextInputState createState() =>
      _TextInputState(controller: controller, validator: validator);
}

class _TextInputState extends State<UsernameFormInput> {
  final TextEditingController controller;
  final FormFieldValidator<String> validator;

  _TextInputState({required this.controller, required this.validator});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextFormField(
          controller: controller,
          keyboardType: TextInputType.text,
          textInputAction: TextInputAction.done,
          decoration: InputDecoration(
            border: const OutlineInputBorder(),
            labelText: 'Username',
            prefixIcon: const Icon(Icons.person),
            suffixIcon: showSuffixIcon(),
          ),
          validator: validator,
        ),
      ],
    );
  }

  Widget showSuffixIcon() {
    return controller.text.isEmpty
        ? Container(width: 0)
        : CloseAndResetButton(onPressed: () => controller.clear());
  }
}
