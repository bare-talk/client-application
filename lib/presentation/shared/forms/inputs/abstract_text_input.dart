import 'package:flutter/material.dart';

abstract class AbstractTextInput extends StatefulWidget {
  final TextEditingController controller;
  final FormFieldValidator<String> validator;

  const AbstractTextInput({
    Key? key,
    required this.controller,
    required this.validator,
  }) : super(key: key);
}
