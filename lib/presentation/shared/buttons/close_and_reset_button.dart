import 'package:flutter/material.dart';

class CloseAndResetButton extends StatelessWidget {
  final Function onPressed;

  const CloseAndResetButton({Key? key, required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.close),
      onPressed: () => onPressed,
    );
  }
}
