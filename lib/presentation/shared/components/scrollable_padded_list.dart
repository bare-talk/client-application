import 'package:client_application/presentation/shared/components/standard_padding.dart';
import 'package:flutter/material.dart';

class ScrollablePaddedList extends StatelessWidget {
  final List<Widget> children;

  const ScrollablePaddedList({Key? key, required this.children})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StandardPadding(
      child: ListView(
        children: children,
      ),
    );
  }
}
