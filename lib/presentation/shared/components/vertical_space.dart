import 'package:flutter/material.dart';

class VerticalSpace extends StatelessWidget {
  final double pixels;

  const VerticalSpace({Key? key, this.pixels = 20.0}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: pixels,
    );
  }
}
