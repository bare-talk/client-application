import 'package:client_application/presentation/screens/login/login_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const BareTalk());
}

class BareTalk extends StatelessWidget {
  const BareTalk({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Bare-Talk',
      home: LoginScreen(),
    );
  }
}
